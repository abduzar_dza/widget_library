import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

///Data model to link with switch
class SwitchData {
  ///constructor
  SwitchData({
    this.data,
    this.state,
  });

  /// Some linked data
  dynamic data;

  /// Default state of switch
  bool state = false;

}

/// Tile with switch at the end ( current platform  switch will be displayed )
class SwitchTile extends StatefulWidget {
  ///constructor
  SwitchTile({
    @required this.text,
    @required this.switchData,
    this.functionality,
    this.decoration,
    this.textStyle,
    this.switchColor,
    Key key,
  }) : super(key: key);

  /// Text of switch
  final String text;

  /// Data model object attached to switch
  final SwitchData switchData;

  /// Default decoration
  final BoxDecoration decoration;

  /// Functionality that will be triggered on switch change
  final Function(bool) functionality;

  /// Default decoration
  final TextStyle textStyle;

  /// Default switch color
  final Color switchColor;

  @override
  _SwitchTileState createState() => _SwitchTileState();
}

class _SwitchTileState extends State<SwitchTile> {
  bool current;

  @override
  void initState() {
    super.initState();
    current = widget.switchData.state;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.decoration,
      child: ListTile(
        title: Text(widget.text, style: widget.textStyle),
        trailing: PlatformSwitch(
          onChanged: (bool value) {
            setState(
                  () {
                current = value;
                if (widget.functionality != null) {
                  widget.functionality(value);
                }
              },
            );
          },
          value: current,
          activeColor: widget.switchColor,
        ),
      ),
    );
  }
}
