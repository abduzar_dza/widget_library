import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Selectable tile widget
class SelectableListTile extends StatefulWidget {
  ///constructor
  SelectableListTile({
    @required Key key,
    @required this.tile,
    @required this.decoration,
    @required this.selectedDecoration,
    this.index,
    this.longPressEnabled,
    this.callback,
    this.isSelected,
  }) : super(key: key);

  /// Tile index
  final int index;

  /// If long press function is available
  final bool longPressEnabled;

  /// Function that will be executed on tap
  final VoidCallback callback;

  /// If tile is selected by default
  final bool isSelected;

  /// Default decoration
  final BoxDecoration decoration;

  /// Decoration to display when tile is selected
  final BoxDecoration selectedDecoration;

  /// Tile to wrap
  final ListTile tile;

  @override
  _SelectableTileState createState() => _SelectableTileState();
}

class _SelectableTileState extends State<SelectableListTile> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        if (widget.longPressEnabled) {
          widget.callback();
        }
      },
      onTap: () {
        widget.callback();
      },
      child: Container(
        decoration:
            widget.isSelected ? widget.selectedDecoration : widget.decoration,
        child: widget.tile,
      ),
    );
  }
}
