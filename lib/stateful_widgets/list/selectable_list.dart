import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:widget_library/stateful_widgets/tiles/selectable_tile.dart';

///Selectable List of provided tiles and linked data
class SelectableList<T> extends StatefulWidget {
  ///constructor
  SelectableList({
    Key key,
    this.data,
    @required this.tiles,
    @required this.decoration,
    @required this.selectedDecoration,
  }) : super(key: key);

  /// Some data that will be passed
  final List<T> data;

  /// List of tiles to display
  final List<ListTile> tiles;

  /// Default tile decoration
  final BoxDecoration decoration;

  /// Selected tile decoration
  final BoxDecoration selectedDecoration;

  @override
  _SelectableListState<T> createState() => _SelectableListState<T>();
}

class _SelectableListState<T> extends State<SelectableList<T>> {
  bool longPressFlag = false;
  List<SelectableElement<T>> indexList = [];

  void onElementSelected(int index) {
    setState(() {
      indexList[index].isSelected = !indexList[index].isSelected;
    });
  }

  void longPress() {
    setState(() {
      if (indexList.isEmpty) {
        longPressFlag = false;
      } else {
        longPressFlag = true;
      }
    });
  }

  @override
  void initState() {
    for (var i = 0; i < widget.data.length; i++) {
      indexList.add(
          SelectableElement(index: i, data: widget.data[i], isSelected: false));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: indexList.length,
      itemBuilder: (BuildContext context, int index) {
        return SelectableListTile(
          key: UniqueKey(),
          decoration: widget.decoration,
          selectedDecoration: widget.selectedDecoration,
          tile: widget.tiles[index],
          isSelected: indexList[index].isSelected,
          longPressEnabled: longPressFlag,
          callback: () {
            onElementSelected(index);
            longPress();
          },
        );
      },
    );
  }
}

/// Selectable item model
class SelectableElement<T> {
  /// constructor
  SelectableElement({
    @required this.index,
    @required this.isSelected,
    @required this.data,
  });

  /// Is item selected by default
  bool isSelected;

  /// Item index
  int index;

  /// Data that linked to tile
  T data;
}
