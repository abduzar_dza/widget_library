import 'package:flutter/material.dart';
import 'package:widget_library/utils/colors/hex_color.dart';

///Creating custom material color from hex string
class CustomMaterialColor {
  ///Constructor for material color from [hexColorString]
  CustomMaterialColor(String hexColorString) {
    _hexString = hexColorString;
    _color = HexColor(hexColorString);
  }

  String _hexString;
  MaterialColor _materialColor;
  Color _color;

  ///Getter for [MaterialColor]
  MaterialColor get materialColor {
    _materialColor = MaterialColor(
      HexColor.getColorFromHex(_hexString),
      _lumenMap(
        HexColor(_hexString),
      ),
    );
    return _materialColor;
  }

  ///Getter for [Color]
  Color get color {
    return _color;
  }

  Map<int, Color> _lumenMap(Color color) {
    return {
      50: Color.fromRGBO(color.red, color.green, color.blue, .1),
      100: Color.fromRGBO(color.red, color.green, color.blue, .2),
      200: Color.fromRGBO(color.red, color.green, color.blue, .3),
      300: Color.fromRGBO(color.red, color.green, color.blue, .4),
      400: Color.fromRGBO(color.red, color.green, color.blue, .5),
      500: Color.fromRGBO(color.red, color.green, color.blue, .6),
      600: Color.fromRGBO(color.red, color.green, color.blue, .7),
      700: Color.fromRGBO(color.red, color.green, color.blue, .8),
      800: Color.fromRGBO(color.red, color.green, color.blue, .9),
      900: Color.fromRGBO(color.red, color.green, color.blue, 1),
    };
  }
}
