import 'dart:ui';

/// Class to create [Color] from hex string
class HexColor extends Color {
  ///Constructor creates [Color] from hex string
  HexColor(final String hexColor) : super(getColorFromHex(hexColor));

  ///Providing integer value of color for creating [Color]
  static int getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }
}
