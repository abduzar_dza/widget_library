import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';

/// Supported locales list
const supportedLocales = ["en", "ru"];

///String localization support
class AppLocalizations {
  ///constructor
  AppLocalizations(this.locale);

  ///Logger
  final Logger _logger = Logger();

  ///Passed locale instance
  final Locale locale;
  Map<String, String> _localizedStrings;

  /// Delegate instance
  static const LocalizationsDelegate<AppLocalizations> delegate =
  _AppLocalizationsDelegate();

  /// Helper method to keep the code in the widgets concise
  /// Localizations are accessed using an InheritedWidget "of" syntax
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  /// Load the language JSON file from the "lib/lang" folder
  Future<void> load() async {
    _logger.d("loading data:: from lib/lang/${locale.languageCode}.json");
    var jsonString =
    await rootBundle.loadString('lib/lang/${locale.languageCode}.json');
    var jsonMap = jsonDecode(jsonString) as Map<String, dynamic>;
    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });
  }

  /// This method will be called from every widget which needs a localized text
  String translate(String key) {
    if (_localizedStrings.containsKey(key)) {
      return _localizedStrings[key];
    } else {
      _logger.w("error:: No translation for $key in ${locale.languageCode}");
      return key;
    }
  }
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return supportedLocales.contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    var appLocalizations = AppLocalizations(locale);
    await appLocalizations.load();
    return appLocalizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
