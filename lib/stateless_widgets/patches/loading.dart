import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

///Wrapping widget to wait until snapshot is loaded
class LoadingWidget<T> extends StatelessWidget {
  ///Constructor
  LoadingWidget({@required this.snapshot, @required this.function});

  /// Snapshot from [FutureBuilder]
  final AsyncSnapshot<T> snapshot;

  /// Function that returns [Widget]
  final Widget Function(AsyncSnapshot<T>) function;

  @override
  Widget build(BuildContext context) {
    if (snapshot.data == null) {
      return Container(
        color: const Color(0xff000000),
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return function(snapshot);
    }
  }
}
