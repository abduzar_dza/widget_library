import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Simple tile with text
class TextTile extends StatelessWidget {
  ///constructor
  TextTile({
    Key key,
    @required this.text,
    @required this.style,
    @required this.decoration,
  }) : super(key: key);

  /// Tile text
  final String text;

  /// Default text stile
  final TextStyle style;

  /// Default decoration
  final BoxDecoration decoration;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      child: ListTile(
        title: Center(
          child: Text(
            text,
            style: style,
          ),
        ),
      ),
    );
  }
}
