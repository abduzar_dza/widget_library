import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';

typedef ValidationRule = ValidationResult Function();

///Validation result object
class ValidationResult {
  ///constructor
  ValidationResult({
    @required this.isValid,
    @required this.message,
  });

  /// Passed check [isValid]
  final bool isValid;

  /// Passed [message]
  final String message;
}

mixin ValidatedWidget on Widget {
  /// Mixin [logger]
  Logger logger = Logger();

  ///Get [valid]
  bool get valid => validate();

  ///Get validation [messages]
  List<String> get messages {
    var messages = [];
    for (var result in validationResults) {
      messages.add(result.message);
    }
    return messages as List<String>;
  }

  ///Rules [validationRules] list
  final List<ValidationRule> validationRules = [];

  ///Result of execution of [validationRules]
  List<ValidationResult> validationResults = [];

  ///Validate widget
  bool validate() {
    for (var validationRule in validationRules) {
      var result = validationRule();
      logger.d(
          "validation result:: ${result.isValid} with message: ${result
              .message}");
      validationResults.add(result);
    }
    logger.d("validation results:: has ${validationResults.length} size");
    if (_checkValidationResults == validationResults.length) {
      return true;
    } else {
      return false;
    }
  }

  int _checkValidationResults() {
    var filtered = validationResults.where((result) => result.isValid);
    logger
        .d("checking validation results:: filtered size is ${filtered.length}");
    return filtered.length;
  }
}
